(function ($) {
  $.widget('vilintritenmert.extendedWeekCalendar', $.ui.weekCalendar, {
    getEventDurationFromPositionEventElement: function (
      $weekDay, $calEvent, top) {
      return this._getEventDurationFromPositionedEventElement($weekDay,
        $calEvent, top);
    },

    refreshEventDetailsInRealTimeWithoutSaving: function (
      calEvent, $calEvent) {
      var suffix = '';
      if (!this.options.readonly &&
        this.options.allowEventDelete &&
        this.options.deletable(calEvent, $calEvent)) {
        suffix = '<div class="wc-cal-event-delete ui-icon ui-icon-close"></div>';
      }
      $calEvent.find('.wc-time').
        html(this.options.eventHeader(calEvent, this.element) + suffix);
      $calEvent.find('.wc-title').
        html(this.options.eventBody(calEvent, this.element));
      $calEvent.data('calEvent', calEvent);
      this.options.eventRefresh(calEvent, $calEvent);
    },
  });
})(jQuery);