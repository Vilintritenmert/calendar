(function ($) {
  var year = new Date().getFullYear();
  var month = new Date().getMonth();
  var day = new Date().getDate();

  var eventData = {
    events: [
      {
        'id': 1,
        'start': new Date(year, month, day, 12),
        'end': new Date(year, month, day, 13, 35),
        'title': 'Lunch with Mike',
      },
      {
        'id': 2,
        'start': new Date(year, month, day, 14),
        'end': new Date(year, month, day, 14, 45),
        'title': 'Dev Meeting',
      },
      {
        'id': 3,
        'start': new Date(year, month, day + 1, 18),
        'end': new Date(year, month, day + 1, 18, 45),
        'title': 'Hair cut',
      },
      {
        'id': 4,
        'start': new Date(year, month, day - 1, 8),
        'end': new Date(year, month, day - 1, 9, 30),
        'title': 'Team breakfast',
      },
      {
        'id': 5,
        'start': new Date(year, month, day + 1, 14),
        'end': new Date(year, month, day + 1, 15),
        'title': 'Product showcase',
      },
    ],
  };


  $.widget('vilintritenmert.mayaCalendar', {
    _eventCalendar: null,
    _calendar: null,
    _create: function () {
      this.element.css('display', 'flex');

      this._initCalendar();
      this._initEventCalendar();
    },
    _initCalendar: function () {
      this._calendar = $('<div class=\"mc-calendar\"></div>');
      this.element.append(this._calendar);
      this._calendar.datepicker({
        onSelect: (dateText, event) => {
          this._eventCalendar.extendedWeekCalendar('gotoDate',
            new Date(dateText));
        },
      });
    },
    _initEventCalendar: function () {
      this._eventCalendar = $('<div class=\"mc-event-calendar\"></div>');
      this.element.append(this._eventCalendar);
      this._eventCalendar.extendedWeekCalendar({
        timeslotsPerHour: 6,
        timeslotHeigh: 30,
        hourLine: true,
        data: eventData,
        buttons: false,
        height: function ($calendar) {
          return $(window).height() - $('h1').outerHeight(true);
        },
        eventAfterRender: (calEvent, $event) => {

          $event.draggable({
            helper: function (event) {
              var $dragged = $(event.currentTarget);
              var $helper = $dragged.clone().removeAttr('id');

              $dragged.addClass('ui-dragged').css('opacity', 0.5);

              return $helper;
            },
            drag: (event) => {
              var $weekDayOnCursorPosition = $('.wc-day-column-inner:hover');

              if ($weekDayOnCursorPosition.length > 0) {
                var $draggedEvent = $('.ui-dragged');
                var $helperEvent = $('.ui-draggable-dragging');
                var top = $helperEvent[0].offsetTop;
                var updatedDate = this._eventCalendar.extendedWeekCalendar(
                  'getEventDurationFromPositionEventElement',
                  $weekDayOnCursorPosition, $draggedEvent, top);

                var calEvent = $draggedEvent.data('calEvent');
                var newCalEvent = $.extend(true, {}, calEvent,
                  {start: updatedDate.start, end: updatedDate.end});

                this._eventCalendar.extendedWeekCalendar(
                  'refreshEventDetailsInRealTimeWithoutSaving',
                  newCalEvent, $draggedEvent);

                this._eventCalendar.extendedWeekCalendar(
                  'refreshEventDetailsInRealTimeWithoutSaving',
                  newCalEvent, $helperEvent);

              }

              return event;
            },
            opacity: 1,
          });
        },
        eventHeader: function (calEvent, calendar) {
          return calEvent.title;
        },
        eventBody: function (calEvent, calendar) {
          var options = calendar.extendedWeekCalendar('option');
          var one_hour = 3600000;
          var displayTitleWithTime = calEvent.end.getTime() -
            calEvent.start.getTime() <= (one_hour / options.timeslotsPerHour);
          if (displayTitleWithTime) {
            return calendar.extendedWeekCalendar(
              'formatTime', calEvent.start) +
              ': ' + calEvent.title;
          } else {
            return calendar.extendedWeekCalendar(
              'formatTime', calEvent.start) +
              options.timeSeparator +
              calendar.extendedWeekCalendar(
                'formatTime', calEvent.end);
          }
        },
      });

    },
  });

})(jQuery);